import os


class Environment():
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'zEMfkABulafmzEMfkABulafmzEMf'
    SESSION_EXPIRATION = 43200
    SSL_DISABLE = False
    JSON_SORT_KEYS = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_POOL_SIZE = 5
    SQLALCHEMY_POOL_TIMEOUT = 10
    SQLALCHEMY_POOL_RECYCLE = 10
    ROOT_USERNAME = "gavrilov-a"
    ROOT_PASSWORD = "aiK9ANoo"
    ROOT_EMAIL = "gavrilov@malltshik.ru"
    MAIL_SERVER = 'smtp.beget.ru'
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_USERNAME = 'noreply@malltshik.ru'
    MAIL_PASSWORD = 'aiK9ANoo'

    @classmethod
    def extended(cls, app):
        pass

class Production(Environment):

    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://mallts_prod:aiK9ANoo@localhost/mallts_prod"

    @classmethod
    def extended(cls, app):
        super(Production, cls).extended(app)
        pass


class Development(Environment):

    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://mallts_dev:aiK9ANoo@mallts.beget.tech/mallts_dev"
    DEBUG = True

class Testing(Environment):
    TESTING = True
    WTF_CSRF_ENABLED = False
    SQLALCHEMY_DATABASE_URI = "sqlite:///:memory:"

envs = {
    'production': Production,
    'development': Development,
    'testing': Testing,
    'default': Development
}
