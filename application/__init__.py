import os
from flask import Flask
from importlib import import_module
from application.configurations import envs
from application.models import db
from flask_mail import Mail

mail = Mail()


def build(env):
    """Build application context."""
    app = Flask(__name__)
    app.template_folder = 'static'
    app.config.from_object(envs[env])
    envs[env].extended(app)
    db.app = app
    db.init_app(app)
    register_blueprints(app)
    mail.app = app
    mail.init_app(app)
    return app


def register_blueprints(app):
    """Load all controllers and register him in app context."""
    filenames = (
        name for name in os.listdir(
            app.root_path + '/controllers') if name.endswith('_controller.py')
    )
    for filename in filenames:
        module = app.name + '.controllers.' + filename.replace('.py', '')
        ctrls = import_module(module)
        ctrl = ctrls.__getattribute__('ctrl')
        app.register_blueprint(ctrl)
