from werkzeug.security import check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from functools import wraps
from flask import current_app, request, g, make_response, jsonify, abort
from application.models.user import User

def verify_password(user, password):
    return check_password_hash(user._password_hash, password)

def generate_session_for_user(user):
    s = Serializer(
        current_app.config['SECRET_KEY'],
        expires_in=current_app.config['SESSION_EXPIRATION'])
    return s.dumps({'id': user.id})

def get_session_user(token):
    s = Serializer(current_app.config['SECRET_KEY'])
    try: data = s.loads(token)
    except: return None
    return User.query.get(data['id'])

def require_session(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = request.cookies.get('session')
        if token:
            user = get_session_user(token)
            if user:
                g.current_user = user
                return f(*args, **kwargs)
            else:
                g.current_user = None
                abort(make_response(jsonify({
                    "message":"The operation requires authentication!"
                    }), 401))
        else:
            g.current_user = None
            abort(make_response(jsonify({
                "message":"The operation requires authentication!"
                }), 401))
    return decorator

def require_user(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = request.cookies.get('session')
        if token:
            user = get_session_user(token)
            if user:
                g.current_user = user
                return f(*args, **kwargs)
            else:
                g.current_user = None
                return f(*args, **kwargs)
        else:
            g.current_user = None
            return f(*args, **kwargs)
    return decorator
