from flask import Blueprint, jsonify, request, make_response
from application.models.todo import Todo
from application.models import db
from application.services.security import require_session
from application.exceptions import ValidationException

ctrl = Blueprint('todos', __name__, url_prefix="/rest/todos")


@ctrl.route('/', methods=["GET"])
@require_session
def query():
    return jsonify([todo.json for todo in Todo.query.all()])


@ctrl.route("/<int:id>/", methods=["GET"])
@require_session
def get(id):
    return jsonify(Todo.query.get_or_404(id).json)


@ctrl.route("/", methods=["POST"])
@require_session
def save():
    try:
        todo = Todo()
        todo.json = request.json
        db.session.add(todo)
        db.session.commit()
        return make_response(jsonify(todo.json), 201)
    except ValidationException as e:
        return make_response(jsonify({"message": e.message}), 409)


@ctrl.route("/<int:id>/", methods=["PUT"])
@require_session
def update(id):
    try:
        todo = Todo.query.get_or_404(id)
        todo.json = request.json
        db.session.commit()
        return jsonify(todo.json)
    except ValidationException as e:
        return make_response(jsonify({"message": e.message}), 409)


@ctrl.route('/<int:id>/', methods=['DELETE'])
@require_session
def delete(id):
    todo = Todo.query.get_or_404(id)
    db.session.delete(todo)
    db.session.commit()
    return make_response(jsonify(), 204)
