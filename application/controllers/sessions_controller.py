#!/usr/bin/env python
"""user_controller module.

Documentation.
"""
from flask import Blueprint
from application.services.security import require_session, generate_session_for_user
from application.services.security import verify_password
from flask import g, jsonify, abort, request, make_response, current_app
from datetime import datetime, timedelta
from application.models.user import User

ctrl = Blueprint('session', __name__, url_prefix="/rest/sessions")

@ctrl.route('/', methods=['GET'])
@require_session
def get():
    return jsonify({'session': True, 'user': g.current_user.json})

@ctrl.route('/', methods=['POST'])
def create():
    """Create new session."""
    username = request.json.get('username')
    password = request.json.get('password')
    if not username or not password:
        abort(401)
    user = User.query.filter_by(username=username).first()
    if not user:
        return make_response(jsonify({
            'message':'User with username ' + username + ' does\'t exist!'
            }), 403)
    if not verify_password(user, password):
        return make_response(jsonify({'message':'Incorrect password!'}), 403)
    token = generate_session_for_user(user)
    response = make_response(jsonify({'session': str(token)}), 201)
    expire_date = datetime.now() + timedelta(seconds=current_app.config['SESSION_EXPIRATION'])
    response.set_cookie('session', value=token, expires=expire_date)
    return response
