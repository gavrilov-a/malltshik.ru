from flask import Blueprint, render_template, jsonify, request, make_response
from flask import abort
from application.models.contact import Contact
from application.models import db
from application.services.security import require_session
from application.exceptions import ValidationException

ctrl = Blueprint('contacts', __name__, url_prefix="/rest/contacts")

@ctrl.route('/', methods=["GET"])
def query():
    return jsonify([contact.json for contact in Contact.query.all()])

@ctrl.route("/<int:id>/", methods=["GET"])
def get(id):
    return jsonify(Contact.query.get_or_404(id).json)

@ctrl.route("/", methods=["POST"])
@require_session
def save():
    try:
        contact = Contact()
        contact.json = request.json
        db.session.add(contact)
        db.session.commit()
        return make_response(jsonify(contact.json), 201)
    except ValidationException as e:
        return make_response(jsonify({"message": e.message}), 409)

@ctrl.route("/<int:id>/", methods=["PUT"])
@require_session
def update(id):
    try:
        contact = Contact.query.get_or_404(id)
        contact.json = request.json
        db.session.commit()
        return jsonify(contact.json)
    except ValidationException as e:
        return make_response(jsonify({"message": e.message}), 409)

@ctrl.route('/<int:id>/', methods=['DELETE'])
@require_session
def delete(id):
    """Delete user order by id."""
    contact = Contact.query.get_or_404(id)
    db.session.delete(contact)
    db.session.commit()
    return make_response(jsonify(), 204)
