from flask import Blueprint, render_template, jsonify, request, make_response, request
from flask import abort
from application.models.user import User
from application.services.security import require_session
from application.models import db

ctrl = Blueprint('users', __name__, url_prefix="/rest/users")
@ctrl.route('/', methods=["GET"])
def query():
    is_author = request.args.get('is_author')
    if is_author == "true":
        return jsonify(User.query.filter_by(is_author=True).first().json)
    return jsonify([user.json for user in User.query.all()])

@ctrl.route("/<int:id>/", methods=["GET"])
def get(id):
    return jsonify(User.query.get_or_404(id).json)

@ctrl.route("/", methods=["POST"])
@require_session
def save():
    user = User()
    user.json = request.json
    db.session.add(user)
    db.session.commit()
    return make_response(jsonify(user.json), 201)

@ctrl.route("/<int:id>/", methods=["PUT"])
@require_session
def update(id):
    user = User.query.get_or_404(id)
    user.json = request.json
    db.session.commit()
    return jsonify(user.json)

@ctrl.route('/<int:id>/', methods=['DELETE'])
@require_session
def delete(id):
    """Delete user order by id."""
    user = User.query.get_or_404(id)
    db.session.delete(user)
    db.session.commit()
    return make_response(jsonify(), 204)
