from flask import Blueprint, render_template, jsonify, request, make_response
from flask import abort
from application.models.skill import Skill
from application.models import db
from application.services.security import require_session
from application.exceptions import ValidationException

ctrl = Blueprint('skills', __name__, url_prefix="/rest/skills")

@ctrl.route('/', methods=["GET"])
def query():
    return jsonify([skill.json for skill in Skill.query.all()])

@ctrl.route("/<int:id>/", methods=["GET"])
def get(id):
    return jsonify(Skill.query.get_or_404(id).json)

@ctrl.route("/", methods=["POST"])
@require_session
def save():
    try:
        skill = Skill()
        skill.json = request.json
        db.session.add(skill)
        db.session.commit()
        return make_response(jsonify(skill.json), 201)
    except ValidationException as e:
        return make_response(jsonify({"message": e.message}), 409)

@ctrl.route("/<int:id>/", methods=["PUT"])
@require_session
def update(id):
    try:
        skill = Skill.query.get_or_404(id)
        skill.json = request.json
        db.session.commit()
        return jsonify(skill.json)
    except ValidationException as e:
        return make_response(jsonify({"message": e.message}), 409)

@ctrl.route('/<int:id>/', methods=['DELETE'])
@require_session
def delete(id):
    """Delete user order by id."""
    skill = Skill.query.get_or_404(id)
    db.session.delete(skill)
    db.session.commit()
    return make_response(jsonify(), 204)
