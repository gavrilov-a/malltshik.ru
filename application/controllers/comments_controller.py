from flask import Blueprint, render_template, jsonify, request
from flask import abort, make_response, redirect, url_for, flash, g
from application.models.comment import Comment
from application.models import db
from application.services.security import require_session, require_user
from application.exceptions import ValidationException

ctrl = Blueprint('comments', __name__, url_prefix="/rest/comments")

@ctrl.route('/', methods=["GET"])
def query():
    post_id = request.args.get("post")
    if not post_id:
        return make_response(\
            jsonify({"message": "Missing 'post' url parameter"}), 409)
    return jsonify([comment.json for comment in \
        Comment.query.filter_by(is_verified=True, post_id=post_id).all()])

@ctrl.route("/<int:id>/", methods=["GET"])
def get(id):
    comment = Comment.query.get_or_404(id)
    token = request.args.get("token")
    if comment.is_verified:
        return jsonify(comment.json)
    elif not token:
        return make_response(jsonify({"message": "Comment is not verified!"}), 403)
    comment = Comment.verified(token)
    if not comment:
        return make_response(jsonify({"message": "Token is not valid!"}), 403)
    return redirect(request.url_root + "posts/" + str(comment.post.id), 302)


@ctrl.route("/", methods=["POST"])
@require_user
def save():
    try:
        comment = Comment()
        comment.json = request.json
        db.session.add(comment)
        db.session.commit()
    except ValidationException as e:
        return make_response(jsonify({"message": e.message}), 409)
    print(bool(g.current_user))
    if g.current_user:
        comment.is_verified = True
        db.session.commit()
        return make_response(jsonify(comment.json), 201)
    else:
        comment.generate_token_and_sand_mail()
        return make_response(jsonify({"message": "Follow the link from mail for confirm comment"}), 201)

@ctrl.route("/<int:id>/", methods=["PUT"])
@require_session
def update(id):
    try:
        comment = Comment.query.get_or_404(id)
        comment.json = request.json
        db.session.commit()
        return jsonify(comment.json)
    except ValidationException as e:
        return make_response(jsonify({"message": e.message}), 409)

@ctrl.route('/<int:id>/', methods=['DELETE'])
@require_session
def delete(id):
    """Delete user order by id."""
    comment = Comment.query.get_or_404(id)
    db.session.delete(comment)
    db.session.commit()
    return make_response(jsonify(), 204)
