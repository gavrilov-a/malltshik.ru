from flask import Blueprint, render_template, jsonify, url_for
from flask import request
from werkzeug.contrib.atom import AtomFeed
from application.models.post import Post
from application.models.user import User

ctrl = Blueprint('pages', __name__)

@ctrl.route('/', methods=['GET'])
def root():
    return render_template("templates/root.html")

@ctrl.route('/rest/', methods=['GET'])
def rest():
    return jsonify({"version":"0.1", "resources": [
        {"user":"link"},
        {"todo":"link"},
    ]})

@ctrl.route('/feed', methods=['GET'])
def rss():
    feed = AtomFeed('Last 10 posts of malltshik blog!', feed_url=request.url, url=request.url_root)
    posts = Post.query.order_by(Post.created.desc()).limit(10).all()
    author = User.query.filter_by(is_author=True).first()
    for post in posts:
        feed.add(post.title, str(post.body), content_type='markdown',
                 author=author.full_name,
                 url= request.url_root + "posts/" + str(post.id),
                 updated=post.updated,
                 published=post.created)
    return feed.get_response()
