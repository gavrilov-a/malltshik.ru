from flask import Blueprint, render_template, jsonify, request, make_response
from flask import abort
from application.models.sphere import Sphere
from application.models import db
from application.services.security import require_session
from application.exceptions import ValidationException

ctrl = Blueprint('spheres', __name__, url_prefix="/rest/spheres")

@ctrl.route('/', methods=["GET"])
@require_session
def query():
    return jsonify([sphere.json for sphere in Sphere.query.all()])

@ctrl.route("/<int:id>/", methods=["GET"])
@require_session
def get(id):
    return jsonify(Sphere.query.get_or_404(id).json)

@ctrl.route("/", methods=["POST"])
@require_session
def save():
    try:
        sphere = Sphere()
        sphere.json = request.json
        db.session.add(sphere)
        db.session.commit()
        return make_response(jsonify(sphere.json), 201)
    except ValidationException as e:
        return make_response(jsonify({"message":e.message}), 409)

@ctrl.route("/<int:id>/", methods=["PUT"])
@require_session
def update(id):
    try:
        sphere = Sphere.query.get_or_404(id)
        sphere.json = request.json
        db.session.commit()
        return jsonify(sphere.json)
    except ValidationException as e:
        return make_response(jsonify({"message":e.message}), 409)

@ctrl.route('/<int:id>/', methods=['DELETE'])
@require_session
def delete(id):
    sphere = Sphere.query.get_or_404(id)
    db.session.delete(sphere)
    db.session.commit()
    return make_response(jsonify(), 204)
