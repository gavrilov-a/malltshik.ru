from flask import Blueprint, render_template, jsonify, request, make_response
from flask import abort
from application.models.tag import Tag
from application.models import db
from application.services.security import require_session
from application.exceptions import ValidationException

ctrl = Blueprint('tags', __name__, url_prefix="/rest/tags")

@ctrl.route('/', methods=["GET"])
def query():
    return jsonify([tag.json for tag in Tag.query.all()])

@ctrl.route("/<int:id>/", methods=["GET"])
def get(id):
    return jsonify(Tag.query.get_or_404(id).json)

@ctrl.route("/", methods=["POST"])
@require_session
def save():
    try:
        tag = Tag()
        tag.json = request.json
        db.session.add(tag)
        db.session.commit()
        return make_response(jsonify(tag.json), 201)
    except ValidationException as e:
        return make_response(jsonify({"message": e.message}), 409)

@ctrl.route("/<int:id>/", methods=["PUT"])
@require_session
def update(id):
    try:
        tag = Tag.query.get_or_404(id)
        tag.json = request.json
        db.session.commit()
        return jsonify(tag.json)
    except ValidationException as e:
        return make_response(jsonify({"message": e.message}), 409)

@ctrl.route('/<int:id>/', methods=['DELETE'])
@require_session
def delete(id):
    """Delete user order by id."""
    tag = Tag.query.get_or_404(id)
    db.session.delete(tag)
    db.session.commit()
    return make_response(jsonify(), 204)
