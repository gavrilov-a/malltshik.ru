from flask import Blueprint, render_template, jsonify, request, make_response, g
from flask import abort
from application.models.post import Post
from application.models import db
from application.services.security import require_session, require_user
from application.exceptions import ValidationException

ctrl = Blueprint('posts', __name__, url_prefix="/rest/posts")

@ctrl.route('/', methods=["GET"])
@require_user
def query():
    if g.current_user:
        return jsonify(Post.search(request.args, with_drafts=True))
    else:
        return jsonify(Post.search(request.args))

@ctrl.route("/<int:id>/", methods=["GET"])
@require_user
def get(id):
    post = Post.query.get_or_404(id)
    if post.is_draft and not g.current_user:
        return make_response(jsonify(), 404)
    return jsonify(post.json)

@ctrl.route("/", methods=["POST"])
@require_session
def save():
    try:
        post = Post()
        post.json = request.json
        db.session.add(post)
        db.session.commit()
        return make_response(jsonify(post.json), 201)
    except ValidationException as e:
        return make_response(jsonify({"message": e.message}), 409)

@ctrl.route("/<int:id>/", methods=["PUT"])
@require_session
def update(id):
    from flask_sqlalchemy import get_debug_queries
    try:
        post = Post.query.get_or_404(id)
        post.json = request.json
        db.session.commit()
        return jsonify(post.json)
    except ValidationException as e:
        return make_response(jsonify({"message": e.message}), 409)

@ctrl.route('/<int:id>/', methods=['DELETE'])
@require_session
def delete(id):
    """Delete user order by id."""
    post = Post.query.get_or_404(id)
    db.session.delete(post)
    db.session.commit()
    return make_response(jsonify(), 204)
