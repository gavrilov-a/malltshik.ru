from application.models import db, BaseModel, tags_to_post
from application.models.tag import Tag
from application.models.comment import Comment
from application.exceptions import ValidationException
from sqlalchemy import func, or_


class Post(BaseModel):
    __tablename__ = 'post'
    __searchable__ = ['body', 'title']
    title = db.Column(db.String(256), nullable=False, index=True)
    body = db.Column(db.Text(), nullable=False)
    is_draft = db.Column(db.Boolean(), nullable=False, default=False)
    tags = db.relationship('Tag', secondary=tags_to_post,
                           backref=db.backref('posts', lazy='dynamic'))
    comments = db.relationship('Comment', backref='post', lazy='dynamic')

    def _validator(self, json):
        errors = str()
        if not json.get("body"):
            errors += "Body of post is required\n"
        if not json.get("title"):
            errors += "Title of post is required\n"
        return errors

    @property
    def json(self):
        json = {
            "id": self.id,
            "created": self.created.timestamp(),
            "updated": self.updated.timestamp(),
            "body": self.body,
            "title": self.title,
            "isDraft": self.is_draft,
            "tags": [tag.json for tag in self.tags],
            "comments": [c.json for c in self.comments.filter(
                Comment.is_verified).all()]
        }
        return json

    @json.setter
    def json(self, json={}):
        errors = self._validator(json)
        if len(errors) > 0:
            raise ValidationException(errors)
        self.body = json.get("body")
        self.title = json.get("title")
        self.is_draft = json.get("isDraft")
        if json.get("tags"):
            self.tags = Tag.query.filter(
                Tag.id.in_([tag["id"] for tag in json.get("tags")])).all()

    @classmethod
    def search(cls, args=None, with_drafts=False):
        result = {}
        tags = args.getlist("tags")
        string = args.get("string") or ""
        size = int(args.get("size") or 10)
        page = int(args.get("page") or 1)
        result["page"] = page
        result["size"] = size
        query = Post.query.filter(or_(Post.title.contains(string),
            Post.body.contains(string)))
        if not with_drafts:
            query = query.filter(Post.is_draft.is_(False))
        if tags:
            query = query.join(Post.tags).filter(Tag.name.in_(tags))\
                .group_by(Post.id).having(func.count(Tag.id) == len(tags))
        query = query.order_by(Post.id.desc()).paginate(page, size, True)
        result["total"] = query.total
        result["pages"] = query.pages
        result["_items"] = [post.json for post in query.items]
        return result
