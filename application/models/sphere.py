from application.models import db, BaseModel
from application.exceptions import ValidationException

class Sphere(BaseModel):
    __tablename__ = 'sphere'
    name = db.Column(db.String(128), unique=True, nullable=False)
    todos = db.relationship('Todo', backref='sphere', lazy='dynamic')

    def _validator(self, json):
        errors = str()
        if not json.get("name"): errors += "Name of sphere is required\n"
        if Sphere.query.filter_by(name=json.get("name")).first():
            errors += "Name of sphere must be unique\n"
        return errors

    @property
    def json(self):
        return {
            "id": self.id,
            "created": self.created.timestamp(),
            "updated": self.updated.timestamp(),
            "name": self.name,
        }

    @json.setter
    def json(self, json={}):
        errors = self._validator(json)
        if len(errors) > 0: raise ValidationException(errors)
        self.name = json.get("name")
