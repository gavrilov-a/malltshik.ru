from application.models import db, BaseModel
from application.models.sphere import Sphere
from application.exceptions import ValidationException


class Todo(BaseModel):
    __tablename__ = 'todo'
    body = db.Column(db.Text(), nullable=False)
    done = db.Column(db.Boolean(), unique=False, nullable=False, default=False)
    sphere_id = db.Column(db.Integer, db.ForeignKey('sphere.id'))

    def _validator(self, json):
        errors = str()
        if not json.get("body"):
            errors += "Body of todo is required\n"
        return errors

    @property
    def json(self):
        json = {
            "id": self.id,
            "created": self.created.timestamp(),
            "updated": self.updated.timestamp(),
            "body": self.body,
            "done": self.done,
        }
        if self.sphere:
            json["sphere"] = self.sphere.json
        else:
            json["sphere"] = None
        return json

    @json.setter
    def json(self, json={}):
        errors = self._validator(json)
        if len(errors) > 0:
            raise ValidationException(errors)
        self.body = json.get("body")
        self.done = json.get("done")
        if json.get("sphere"):
            self.sphere = Sphere.query.get(json.get("sphere").get("id"))
