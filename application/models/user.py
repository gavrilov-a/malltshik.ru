from flask import current_app
from application.models import db, BaseModel
from werkzeug.security import generate_password_hash

class User(BaseModel):
    __tablename__ = 'user'
    username = db.Column(db.String(64), unique=True, index=True, nullable=False)
    email = db.Column(db.String(128), unique=True, index=True, nullable=False)
    is_author = db.Column(db.Boolean, nullable=False, index=True, default=False)
    first_name = db.Column(db.String(64))
    last_name = db.Column(db.String(64))
    about = db.Column(db.Text())
    _password_hash = db.Column(db.String(128), nullable=False)

    @property
    def full_name(self):
        return self.first_name + " " + self.last_name

    @property
    def password(self):
        """Not readable attribute."""
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        """Password setter."""
        self._password_hash = generate_password_hash(password)

    @property
    def json(self):
        return {
            'id': self.id,
            'created': self.created.timestamp(),
            'updated': self.updated.timestamp(),
            'username': self.username,
            'email': self.email,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'about': self.about,
            'is_author': self.is_author
        }

    @json.setter
    def json(self, json={}):
        self.username = json.get('username')
        self.email = json.get('email')
        self.first_name = json.get('first_name')
        self.last_name = json.get('last_name')
        self.about = json.get('about')
        self.is_author = json.get('is_author')
        if json.get("password"):
            self.password = json['password']
