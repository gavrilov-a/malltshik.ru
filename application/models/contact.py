from application.models import db, BaseModel
from application.exceptions import ValidationException


class Contact(BaseModel):
    __tablename__ = 'contact'
    body = db.Column(db.String(128), nullable=False, unique=True)
    icon = db.Column(db.String(64), unique=False, nullable=False, default="address-card-o")

    def _validator(self, json):
        errors = str()
        c = Contact.query.filter_by(body=json.get("body")).first()
        if not json.get("body"): errors += "Body of contact is required\n"
        if c and c.id != json.get("id"): errors += "Body of contact must be unique\n"
        return errors

    @property
    def json(self):
        json = {
            "id": self.id,
            "created": self.created.timestamp(),
            "updated": self.updated.timestamp(),
            "body": self.body,
            "icon": self.icon,
        }
        return json

    @json.setter
    def json(self, json={}):
        errors = self._validator(json)
        if len(errors) > 0:
            raise ValidationException(errors)
        self.body = json.get("body")
        self.icon = json.get("icon")
