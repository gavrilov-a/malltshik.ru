from application.models import db, BaseModel
from application.exceptions import ValidationException


class Skill(BaseModel):
    __tablename__ = 'skill'
    name = db.Column(db.String(128), nullable=False, unique=True)
    progress = db.Column(db.Integer, unique=False, nullable=False, default=0)

    def _validator(self, json):
        errors = str()
        s = Skill.query.filter_by(name=json.get("name")).first()
        if not json.get("name"):errors += "Name of skill is required\n"
        if s and s.id != json.get("id"): errors += "Name of skill must be unique\n"
        return errors

    @property
    def json(self):
        json = {
            "id": self.id,
            "created": self.created.timestamp(),
            "updated": self.updated.timestamp(),
            "name": self.name,
            "progress": self.progress,
        }
        return json

    @json.setter
    def json(self, json={}):
        errors = self._validator(json)
        if len(errors) > 0:
            raise ValidationException(errors)
        self.name = json.get("name")
        self.progress = json.get("progress")
