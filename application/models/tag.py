from application.models import db, BaseModel, tags_to_post
from application.exceptions import ValidationException

class Tag(BaseModel):
    __tablename__ = 'tag'
    name = db.Column(db.String(128), nullable=False, unique=True, index=True)

    def _validator(self, json):
        errors = str()
        if not json.get("name"): errors += "Name of tag is required\n"
        if Tag.query.filter_by(name=json.get("name")).first():
            errors += "Name of tag must be unique\n"
        return errors

    @property
    def json(self):
        from application.models.post import Post
        json = {
            "id": self.id,
            "created": self.created.timestamp(),
            "updated": self.updated.timestamp(),
            "name": self.name,
            "posts": Post.query.filter(Post.tags.contains(self)).count()
        }
        return json

    @json.setter
    def json(self, json={}):
        errors = self._validator(json)
        if len(errors) > 0: raise ValidationException(errors)
        self.name = json.get("name")
