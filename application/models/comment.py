from application.models import db, BaseModel
from application.exceptions import ValidationException
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app, url_for, render_template, request
from re import match
from flask_mail import Message
from application import mail
from application.models.user import User

class Comment(BaseModel):
    __tablename__ = 'comment'
    email = db.Column(db.String(128), nullable=False)
    author = db.Column(db.String(128), nullable=False, default="Anonimus")
    body = db.Column(db.Text(), nullable=False)
    is_verified = db.Column(db.Boolean(), nullable=False, default=False)
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'))

    def _validator(self, json):
        errors = str()
        if not json.get("email"): errors += "Email of comment is required\n"
        elif not match(
            r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)",
            json.get("email")):
            errors += "Not valid email address\n"
        if not json.get("body"): errors += "Body of comment is required\n"
        if not json.get("post_id"): errors += "Post id of comment is required"
        return errors

    @property
    def json(self):
        json = {
            "id": self.id,
            "created": self.created.timestamp(),
            "updated": self.updated.timestamp(),
            "email": self.email,
            "author": self.author,
            "body": self.body,
            "is_verified" : self.is_verified,
            "post_id": self.post_id
        }
        return json

    @json.setter
    def json(self, json={}):
        errors = self._validator(json)
        if len(errors) > 0: raise ValidationException(errors)
        self.email = json.get("email")
        self.author = json.get("author")
        self.body = json.get("body")
        self.post_id = json.get("post_id")


    @classmethod
    def new_comment_notify(cls, comment):
        subject = "New comment on malltshik.ru"
        sender = current_app.config["MAIL_USERNAME"]
        link = request.url_root + "posts/" + str(comment.post.id)
        recipients = [user.email for user in
            User.query.filter_by(is_author=True).all()]
        body = "Link to post with new comment: {}".format(link)
        html = render_template("templates/mail/new-comment.html",
            comment=comment, link=link, subject=subject)
        m = Message(subject=subject, sender=sender, recipients=recipients,
            body=body, html=html)
        mail.send(m)


    @classmethod
    def verified(cls, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try: data = s.loads(token)
        except: return None
        comment = Comment.query.get(data['id'])
        comment.is_verified = True
        db.session.commit()
        Comment.new_comment_notify(comment)
        return comment

    def generate_token_and_sand_mail(self):
        s = Serializer(current_app.config['SECRET_KEY'])
        link = url_for("comments.get",
            id=self.id,
            token = s.dumps({'id': self.id}),
            _external=True)
        subject = "E-mail confirmation for comment"
        sender = current_app.config["MAIL_USERNAME"]
        recipients = [self.email]
        body = "Link E-mail confirmation for comment: {}".format(link)
        html = render_template("templates/mail/verified-comment.html",
            comment=self, link=link, subject=subject)
        m = Message(subject=subject, sender=sender, recipients=recipients,
            body=body, html=html)
        mail.send(m)
