App.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
  .state('index', {
      url: '/',
      templateUrl: "/static/templates/index/index.html",
      controller: 'IndexController',
      ncyBreadcrumb: {
        label: 'Home',
      },
      access: { requireLogin: false }
    })
  .state('index.posts', {
      url: 'posts?s&tags&page',
      params: { s: null, tags: {value: new Array(), array: true }, page: null },
      templateUrl: "/static/templates/posts/posts.html",
      controller: 'PostsController',
      ncyBreadcrumb: {
        label: 'Posts',
      },
      access: { requireLogin: false }
  })
  .state('index.posts.new', {
      url: '/new',
      params: {},
      templateUrl: "/static/templates/posts/new.html",
      controller: 'PostNewController',
      ncyBreadcrumb: {
        label: 'Create new post',
      },
      access: { requireLogin: true }
  })
  .state('index.posts.post', {
      url: '/:id',
      templateUrl: "/static/templates/posts/post.html",
      controller: 'PostController',
      ncyBreadcrumb: {
        label: 'Post of blog',
      },
      access: { requireLogin: false }
  })
  .state('index.posts.post.edit', {
      url: '/edit',
      templateUrl: "/static/templates/posts/edit.html",
      controller: 'PostEditController',
      ncyBreadcrumb: {
        label: 'Edit post',
      },
      access: { requireLogin: true }
  })
  .state('index.todos', {
      url: 'todos',
      templateUrl: "/static/templates/todos/todos.html",
      controller: 'TodoController',
      ncyBreadcrumb: {
        label: "Todo's",
      },
      access: { requireLogin: true }
  })
  .state('index.settings', {
      url: 'settings',
      templateUrl: "/static/templates/settings/settings.html",
      controller: 'SettingsController',
      ncyBreadcrumb: {
        label: 'Settings',
      },
      access: { requireLogin: true }
    });
    $urlRouterProvider.otherwise("/");
});
