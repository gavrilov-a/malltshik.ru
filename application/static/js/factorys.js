App.factory('Rest', ['$resource', function($resource) {
  return {
    Session: $resource('/rest/sessions/', {}, {}),
    User: $resource('/rest/users/:id/', {id: '@id'}, {'update':{method:'PUT'}}),
    Todo: $resource('/rest/todos/:id/', {id: '@id'}, {'update':{method:'PUT'}}),
    Sphere: $resource('/rest/spheres/:id/', {id: '@id'}, {'update':{method:'PUT'}}),
    Skill: $resource('/rest/skills/:id/', {id: '@id'}, {'update':{method:'PUT'}}),
    Contact: $resource('/rest/contacts/:id/', {id: '@id'}, {'update':{method:'PUT'}}),
    Tag: $resource('/rest/tags/:id/',{id: '@id'}, {'update':{method:'PUT'}}),
    Comment: $resource('/rest/comments/:id/',{id: '@id'}, {'update':{method:'PUT'}}),
    Post: $resource('/rest/posts/:id/',{id: '@id'}, {'update':{method:'PUT'},
      'query': {
        method: 'GET',
        isArray: false
      },
      'page': {
        method: 'GET',
        params: {page: ('@page' || 1)}
      }
    }),
  }
}]);
