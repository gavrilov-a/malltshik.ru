App.controller("HomeController", ["$rootScope", "$scope", "Rest", "ToastService", "clipboard",
function($rootScope, $scope, Rest, ToastService, clipboard) {
  $scope.skills = Rest.Skill.query();
  $scope.contacts = Rest.Contact.query();
  $scope.author = Rest.User.get({is_author:true});
  $scope.posts = Rest.Post.query({limit: 5});
  $scope.copied = function(){ToastService.show(200, 'Copied to clipboard')}
}]);
