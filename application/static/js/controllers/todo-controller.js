App.controller("TodoController", ["$rootScope", "$scope", "Rest", "localStorageService", "ToastService",
function($rootScope, $scope, Rest, localStorageService, ToastService) {

  sQuery = localStorageService.get('todo.sQuery');
  if(sQuery != null) {
    $scope.sQuery = sQuery;
  } else {
    $scope.sQuery = {status: 'all', spheres: []}
  }

  $scope.setSQueryStatus = function(status) {
    $scope.sQuery.status = status;
    localStorageService.set('todo.sQuery', $scope.sQuery)
  }

  $scope.editable = 0;
  $scope.todos = Rest.Todo.query();
  $scope.newTodo = new Rest.Todo();
  $scope.spheres = Rest.Sphere.query();
  $scope.sphereRemovable = false
  $scope.sphereRo = true
  $scope.sphereEditMode = false
  $scope.sphereToChip = function(chip) { return {name: chip}}

  $scope.toggleEditMode = function() {
    $scope.sphereRemovable = !$scope.sphereRemovable
    $scope.sphereRo = !$scope.sphereRo
    $scope.sphereEditMode = !$scope.sphereEditMode
  }

  $scope.removeSphere = function(sphere) {
    sphere.$remove().then(function(success) {
      $scope.todos.foreach(function(item) {
        if(item && item.sphere && item.sphere.name === sphere.name) { item.sphere = null }
      });
      ToastService.show("200", "Sphere successful deleted")
    }, function(error) {
      ToastService.show(error.status, error.data.message)
    });
  }

  $scope.sphereToChip = function(chip) {
    new Rest.Sphere({name:chip}).$save()
    .then(function(response){
      $scope.spheres.push(response)
      $scope.sphereNotUniq = false
      ToastService.show("200", "Sphere successful created!")
    }, function(error) {
      $scope.sphereNotUniq = true
      ToastService.show(error.status, error.data.message)
    });
    return null
  }

  $scope.submit = function(newTodo){
    newTodo.$save().then(function(todo){
      $scope.todos.push(todo)
      $scope.newTodo = new Rest.Todo();
      ToastService.show("200", "Todo successful created")
    }, function(error){
      ToastService.show(error.status, error.data.message)
    });
  }

  $scope.update = function(todo){
    if(!todo.body) { todo = todo.$get() }
    else { todo.$update().then(function(success){
      ToastService.show("200", "Todo successful updated")
    }, function(error){
      ToastService.show(error.status, error.data.message)
    }); }
    $scope.editable = 0;
  }

  $scope.makeAllDone = function(){
    $scope.todos = $scope.todos.filter(function(el){
      if(!el.done){ el.done = true; el.$update(); }
      return true;
    });
    ToastService.show("200", "All todo's marked done")
  }

  $scope.makeAllUnDone = function(){
    $scope.todos = $scope.todos.filter(function(el){
      if(el.done){ el.done = false; el.$update(); }
      return true;
    });
    ToastService.show("200", "All todo's marked not done")
  }

  $scope.removeComplited = function(){
    $scope.todos = $scope.todos.filter(function(el){
      if(el.done){el.$remove(); return false;}
      return true;
    });
    ToastService.show("200", "All doned todo's removed")
  }

  $scope.remove = function(todo){
     todo.$remove().then(function() {
       $scope.todos = $scope.todos.filter(function(el){
         return el.id != todo.id;
       });
       ToastService.show("200", "Todo successful removed")
     }, function(error){
       ToastService.show(error.status, error.data.message)
     });
  }

  $scope.toggleSphere = function(event, chip) {
    chipTemplate = $(event.target.parentElement.parentElement);
    if($scope.sQuery.spheres.indexOf(chip.name) > -1) {
      $scope.sQuery.spheres.splice($scope.sQuery.spheres.indexOf(chip.name), 1)
      chipTemplate.css('background', 'rgb(224,224,224)')
    } else {
      $scope.sQuery.spheres.push(chip.name)
      chipTemplate.css('background', 'rgb(91,192,222)')
    }
    localStorageService.set('todo.sQuery', $scope.sQuery)
  }

  angular.element(document).ready(function () {
    $scope.$watch('sQuery', function() {
      $('md-chip').has('.selected').css('background', 'rgb(91,192,222)');
    });
  });

}]);
