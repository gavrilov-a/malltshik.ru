App.controller("SettingsController", ["$rootScope", "$scope", "Rest", "localStorageService", "ToastService",
function($rootScope, $scope, Rest, localStorageService, ToastService) {


  $scope.author = Rest.User.get({is_author:true});
  $scope.onEditAbout = false
  $scope.editAbout = function(author){
    if(!$scope.onEditAbout) {$scope.onEditAbout=true; return}
    author.$update().then(function(){
      $scope.onEditAbout = false
      ToastService.show("200", "Profile successful updated")
    }, function(error){
      ToastService.show(error.status, error.data.message)
    });
  }
  $scope.cancelEditAbout = function(){
    $scope.onEditAbout=false;
    $scope.author = Rest.Author.get();
  }


  $scope.contacts = Rest.Contact.query();
  $scope.newContact = new Rest.Contact({icon: 'address-card-o'});
  $scope.onEditContact = 0;

  $scope.createContact = function(contact) {
    contact.$save().then(function(){
        $scope.contacts.push(contact)
    }, function(error){
      ToastService.show(error.status, error.data.message)
    });
    $scope.newContact = new Rest.Contact({icon: 'address-card-o'});
  }

  $scope.updateContact = function(contact){
    if(!$scope.onEditContact) {$scope.onEditContact=contact.id; return}
    contact.$update().then(function(){
      $scope.onEditContact = 0;
      ToastService.show("200", "Contact successful updated")
    }, function(error){
      ToastService.show(error.status, error.data.message)
    })
  }

  $scope.removeContact = function(contact){
    contact.$remove().then(function() {
      $scope.contacts = $scope.contacts.filter(function(el){
        return el.id != contact.id;
      });
      ToastService.show("200", "Contact successful removed")
    }, function(error){
      ToastService.show(error.status, error.data.message)
    });
  }

  $scope.skills = Rest.Skill.query();
  $scope.newSkill = new Rest.Skill({progress: 53});
  $scope.onEditSkil = 0;

  $scope.createSkill = function(skill) {
    skill.$save().then(function(){
        $scope.skills.push(skill)
    }, function(error){
      ToastService.show(error.status, error.data.message)
    });
    $scope.newSkill = new Rest.Skill({progress: 53});
  }

  $scope.updateSkill = function(skill){
    if(!$scope.onEditSkill) {$scope.onEditSkill=skill.id; return}
    skill.$update().then(function(){
      $scope.onEditSkill = 0;
      ToastService.show("200", "Skill successful updated")
    }, function(error){
      ToastService.show(error.status, error.data.message)
    })
  }

  $scope.removeSkill = function(skill){
    skill.$remove().then(function() {
      $scope.skills = $scope.skills.filter(function(el){
        return el.id != skill.id;
      });
      ToastService.show("200", "Skill successful removed")
    }, function(error){
      ToastService.show(error.status, error.data.message)
    });
  }

}]);
