App.controller("PostsController", ["$rootScope", "$scope", "Rest",
  "localStorageService", "ToastService", "$state", "$stateParams", "$q",
  function($rootScope, $scope, Rest, localStorageService, ToastService, $state,
  $stateParams, $q) {

  $scope.tags = Rest.Tag.query();
  $scope.tagRemovable = false
  $scope.tagRo = true
  $scope.tagEditMode = false
  $scope.tagToChip = function(chip) { return {name: chip}}

  $scope.sQuery = function() {
    return {
      string: $stateParams.s,
      tags: $stateParams.tags,
      page: $stateParams.page
    }
  }

  $scope.$watch("$stateParams", function(){
    $scope.query = {};
    $scope.query.string = $stateParams.s;
  })

  $scope.posts = Rest.Post.query($scope.sQuery()).$promise
  .then(function(response) {
    $scope.posts = response;
  }, function(error) {
    $scope.posts = {}; $scope.posts._items = [];
  });

  $scope.next = function() {
    $stateParams.page = $scope.posts.page + 1;
    $scope.posts = Rest.Post.query($scope.sQuery());
    $state.go('index.posts', $scope.sQuery(), {notify: false});
  }

  $scope.preview = function(){
    $stateParams.page = $scope.posts.page - 1;
    $scope.posts = Rest.Post.query($scope.sQuery());
    $state.go('index.posts', $scope.sQuery(), {notify: false});
  }

  $scope.search = function(interactive){
    if(interactive) {
      $stateParams.page = 1;
      $stateParams.s = $scope.query.string;
      console.log($stateParams.s, $scope.query.string);
      $scope.posts = Rest.Post.query($scope.sQuery()).$promise
      .then(function(response) {
        $scope.posts = response;
      }, function(error) {
        $scope.posts = {}; $scope.posts._items = [];
      });
      $state.go('index.posts', {
        s: $scope.query.string,
        page:1,
        tags:$stateParams.tags
      }, {notify: false});
    }
  }

  $scope.toggleTag = function(event, chip) {
    chipTemplate = $(event.target.parentElement.parentElement);
    if($stateParams.tags != undefined && $stateParams.tags != null &&
      typeof $stateParams.tags == "string") {
      $stateParams.tags = [$stateParams.tags];
    }
    if($stateParams.tags.indexOf(chip.name) > -1) {
      $stateParams.tags.splice($stateParams.tags.indexOf(chip.name), 1)
      chipTemplate.css('background', 'rgb(224,224,224)')
    } else {
      $stateParams.tags.push(chip.name)
      chipTemplate.css('background', 'rgb(91,192,222)')
    }
    $scope.search(true);
  }

  $scope.toggleEditMode = function() {
    $scope.tagRemovable = !$scope.tagRemovable
    $scope.tagRo = !$scope.tagRo
    $scope.tagEditMode = !$scope.tagEditMode
  }

  $scope.removeTag = function(tag) {
    tag.$remove().then(function(success) {
      ToastService.show("200", "Tag successful deleted")
    }, function(error) {
      ToastService.show(error.status, error.data.message)
    });
  }

  $scope.tagToChip = function(chip) {
    new Rest.Tag({name:chip}).$save()
    .then(function(response){
      $scope.tags.push(response)
      $scope.tagNotUniq = false
      ToastService.show("200", "Tag successful created!")
    }, function(error) {
      $scope.tagNotUniq = true
      ToastService.show(error.status, error.data.message)
    });
    return null
  }

}]);

App.controller("PostController", ["$rootScope", "$scope", "Rest", "$stateParams", "$state",  "$mdDialog", "ToastService",
function($rootScope, $scope, Rest, $stateParams, $state, $mdDialog, ToastService) {

  Rest.Post.get({id:$stateParams.id}).$promise.then(function(response){
    $scope.post = response;
  }, function(error){
    if(error.status == 404){$state.go("index.posts")}
    else {ToastService.show(error.status, error.data.message)}
  });

  $scope.delete = function(ev, post) {
    var confirm = $mdDialog.confirm()
          .title('Would you like to delete your post?')
          .ariaLabel('Lucky day')
          .targetEvent(ev)
          .ok('Yes')
          .cancel('No');

    $mdDialog.show(confirm).then(function() {
      $scope.post.$remove().then(function(){
        $state.go("index.posts", {}, {reload:true})
        ToastService.show("200", "Post successful deleted!")
      }, function(error){
        ToastService.show(error.status, error.data.message)
      })
    }, function() { console.log("Confirm canceled!") });
  };

  $scope.copyLink = function(){
    ToastService.show("200", "Link copied to clipboard!")
  }

  $scope.removeComment = function(event, comment){
    var confirm = $mdDialog.confirm()
          .title('Would you like to delete this comment?')
          .ariaLabel('Lucky day')
          .targetEvent(event)
          .ok('Yes')
          .cancel('No');

    $mdDialog.show(confirm).then(function() {
      cmnt = new Rest.Comment(comment)
      cmnt.$remove().then(function() {
        ToastService.show("200", "Comment successful removed")
        comment.id = 0
      }, function(error){
        ToastService.show(error.status, error.data.message)
      });
    }, function() { console.log("Confirm canceled!") });
  }

  $scope.newComment = new Rest.Comment();

  $scope.sendComment = function (comment){
    comment.post_id = $scope.post.id;
    if($rootScope.$currentUser) {
      comment.email = $rootScope.$currentUser.email
      comment.author = $rootScope.$currentUser.last_name +
        " " +$rootScope.$currentUser.first_name;
    }
    comment.$save().then(function(responce) {
      if(responce.id){
        $scope.post.comments.push(responce)
        ToastService.show("200", "Comment successful added!")
      } else {
        ToastService.show("200", responce.message, 5000)
      }
      $scope.newComment = new Rest.Comment();
    }, function(error){
      ToastService.show(error.status, error.data.message)
    })
  }

}]);

App.controller("PostEditController", ["$rootScope", "$scope", "Rest", "$stateParams", "$state",  "$mdDialog", "ToastService",
function($rootScope, $scope, Rest, $stateParams, $state, $mdDialog, ToastService) {
  $scope.post = Rest.Post.get({id:$stateParams.id});
  $scope.tags = new Rest.Tag.query();

  $scope.updateNewPost = function(post){
    post.$update().then(function(responce){
      ToastService.show("200", "Post successful updated!")
      $state.go("index.posts.post", {id: responce.id}, {reload: true})
    }, function(error){
      ToastService.show(error.status, error.data.message)
    })
  }

  $scope.previewNewPost = function(ev, post){
    if(!post.title || !post.body) {return}
    $mdDialog.show({
        controller: DialogController,
        templateUrl: '/static/templates/posts/new-preview-dialog.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        scope: $scope.$new(),
        clickOutsideToClose:false,
        fullscreen: true
      })
      .then(function(answer) { $scope.updateNewPost(answer) },
      function() { console.log("Back to edit")}
    )}

  function DialogController($scope, $mdDialog) {
    $scope.cancel = function() { $mdDialog.cancel() };
    $scope.submit = function(answer, isDraft) {
      answer.isDraft = isDraft; 
      $mdDialog.hide(answer);
    };
  }

  $scope.selectedTag = null;
  $scope.searchText = null;

  function transformChip(chip) {
    return $scope.tags.filter(function(i){i.name == chip})[0]
  }

  $scope.querySearch = function(query) {
    return $scope.tags.filter(function(item){return item.name.toLowerCase().indexOf(query.toLowerCase()) === 0 });
  }

}]);

App.controller("PostNewController", ["$rootScope", "$scope", "Rest", "ToastService", "$mdDialog", "$state",
function($rootScope, $scope, Rest, ToastService, $mdDialog, $state) {

  $scope.post = new Rest.Post();
  $scope.tags = new Rest.Tag.query();

  $scope.createNewPost = function(post){
    post.$save().then(function(responce){
      ToastService.show("200", "Post successful created!")
      $state.go("index.posts.post", {id: responce.id},  {reload: true})
    }, function(error){
      ToastService.show(error.status, error.data.message)
    })
  }

  $scope.previewNewPost = function(ev, post){
    if(!post.title || !post.body) {return}
    $mdDialog.show({
        controller: DialogController,
        templateUrl: '/static/templates/posts/new-preview-dialog.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        scope: $scope.$new(),
        clickOutsideToClose:false,
        fullscreen: true
      })
      .then(function(answer) { $scope.createNewPost(answer) },
      function() {
        console.log("Back to edit")
      }
    )}

  function DialogController($scope, $mdDialog) {
    $scope.cancel = function() { $mdDialog.cancel() };
    $scope.submit = function(answer, isDraft) {
      answer.isDraft = isDraft; 
      $mdDialog.hide(answer);
    };
  }

  $scope.post.tags = [];
  $scope.selectedTag = null;
  $scope.searchText = null;

  function transformChip(chip) {
    return $scope.tags.filter(function(i){i.name == chip})[0]
  }

  $scope.querySearch = function(query) {
    return $scope.tags.filter(function(item){return item.name.toLowerCase().indexOf(query.toLowerCase()) === 0 });
  }

}]);
