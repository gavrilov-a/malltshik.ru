App.controller("IndexController", ["$rootScope", "$scope", "$mdSidenav", "$mdDialog", "Rest", "$state",
function($rootScope, $scope, $mdSidenav, $mdDialog, Rest, $state) {

  $scope.toggleSideBar = function(){
    $mdSidenav('left').toggle();
  }

  $scope.showLoginModal = function(ev) {
    $mdDialog.show({
      controller: LoginDialogController,
      templateUrl: '/static/templates/dialogs/login.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
    })
  };

  function LoginDialogController($scope, $mdDialog, Rest) {
    $scope.creditional = new Rest.Session();
    $scope.hide = function() {
      $mdDialog.hide();
    };

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.submit = function() {
      if(!$scope.loginForm.$valid) {
        $scope.error = "Invalid form!"
        return;
      }
      $scope.creditional.$save(function(success) {
        $mdDialog.hide();
        $state.reload()
      }, function(error) {
        $scope.error = error.data.message
      })
    };
  }



}]);
