App.filter('todoSearch', function () {
  return function (todos, sQuery) {
    if (!sQuery) { return todos; }
    result = []
    todos.foreach(function(todo){
      if(!todo){return}
      if( (sQuery.spheres.length <= 0 || (todo.sphere && sQuery.spheres.indexOf(todo.sphere.name) > -1)) &&
      ((sQuery.status == 'done' && todo.done) || (sQuery.status == 'todo' && !todo.done) || sQuery.status == 'all') ) {
        result.push(todo)
      }
    });
    return result;
  }
});
