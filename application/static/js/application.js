var App = angular.module("App", [
  'ui.router',
  'ui.gravatar',
  'ncy-angular-breadcrumb',
  'ngMaterial',
  'ngResource',
  'ngMessages',
  'ngCookies',
  'ngSanitize',
  'ngAnimate',
  'angular-loading-bar',
  'LocalStorageModule',
  'angular-clipboard',
  'hc.marked',
  'truncate'
]);

console.info("App successful started!")

App.run(['$rootScope', '$state', '$stateParams', 'SessionService', 'FlashiesService',
function ($rootScope, $state, $stateParams, SessionService, FlashiesService) {

    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.$flashies = $flashies;
    FlashiesService.flash();

    $rootScope.$sapply = function(fn) {
      var phase = this.$root.$$phase;
      if(phase == '$apply' || phase == '$digest') {
        if(fn && (typeof(fn) === 'function')) { fn() }
      } else {
        this.$apply(fn);
      }
    }

    $rootScope.$on('$stateChangeStart',
    function(event, toState, toParams, fromState, fromParams, options) {
      SessionService.check(event, toState, toParams, fromState, fromParams, options);
    });

}]);

Array.prototype.foreach = function (callback) {
  iterations = Math.floor(this.length / 5);
  len = this.length
  if(len == 0) {return this}
  dot = len % 5;
  i = 0;
  do { switch (dot) {
      case 0: callback(this[i++]);
      case 4: callback(this[i++]);
      case 3: callback(this[i++]);
      case 2: callback(this[i++]);
      case 1: callback(this[i++]);
    }
    dot = 0
  } while (iterations--)
  return this;
};

Array.prototype.contains = function (item) {
  return this.includes(item)
};
