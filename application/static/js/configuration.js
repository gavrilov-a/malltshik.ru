App.config(function($resourceProvider, gravatarServiceProvider, cfpLoadingBarProvider, $locationProvider,
  $mdThemingProvider, $breadcrumbProvider, localStorageServiceProvider) {

  // URL SLASH
  $resourceProvider.defaults.stripTrailingSlashes = false;
  $locationProvider.html5Mode(true);

  // GRAVATAR
  gravatarServiceProvider.defaults = {
    size     : 100,
    "default": 'identicon'
  };
  gravatarServiceProvider.secure = true;

  // LOCAL STORAGE
  localStorageServiceProvider.setPrefix('malltshikApp');

  // LOADING BAR
  cfpLoadingBarProvider.includeSpinner = false;

  // MD PALETTE
  var customPrimary = {
    '50': '#5bc0de',
    '100': '#5bc0de',
    '200': '#5bc0de',
    '300': '#5bc0de',
    '400': '#5bc0de',
    '500': '#5bc0de',
    '600': '#5bc0de',
    '700': '#5bc0de',
    '800': '#5bc0de',
    '900': '#5bc0de',
    'A100': '#5bc0de',
    'A200': '#5bc0de',
    'A400': '#5bc0de',
    'A700': '#5bc0de'
  };
  $mdThemingProvider.definePalette('primary', customPrimary);

  var customAccent = {
    '50': '#1b6d85',
    '100': '#1f7e9a',
    '200': '#2390b0',
    '300': '#28a1c5',
    '400': '#31b0d5',
    '500': '#46b8da',
    '600': '#70c8e2',
    '700': '#85d0e7',
    '800': '#9bd8eb',
    '900': '#b0e1ef',
    'A100': '#70c8e2',
    'A200': '#5bc0de',
    'A400': '#46b8da',
    'A700': '#c5e9f3'
  };
  $mdThemingProvider.definePalette('accent', customAccent);

  $mdThemingProvider.theme('default')
  .primaryPalette('primary')
  .accentPalette('accent')
  .warnPalette('deep-orange')

});


App.config(['markedProvider', function (markedProvider) {
  markedProvider.setOptions({
    gfm: true,
    tables: true,
    highlight: function (code, lang) {
      if (lang) {
        return hljs.highlight(lang, code, true).value;
      } else {
        return hljs.highlightAuto(code).value;
      }
    }
  });
}]);
