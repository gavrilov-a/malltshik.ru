App.service('SessionService', ['$rootScope', '$state', '$cookies', 'Rest',
function($rootScope, $state, $cookies, Rest){

  $rootScope.$signOut = function() {
    $rootScope.$currentUser = undefined
    $rootScope.$isLogin = undefined;
    $cookies.remove('session');
    $state.reload()
  };

  this.check = function(event, toState, toParams, fromState, fromParams, options) {
    Rest.Session.get().$promise.then( function(response) {
      $rootScope.$currentUser = new Rest.User(response.user);
      $rootScope.$isLogin = response.session;
      $rootScope.changeRoute(event, response, toState, toParams, fromState, fromParams)
    }, function(error) {
      $rootScope.$currentUser = undefined;
      $rootScope.$isLogin = undefined;
      $rootScope.changeRoute(event, null, toState, toParams, fromState, fromParams)
      console.warn('UNAUTHORIZED');
    });
  }

  $rootScope.changeRoute = function(event, response, toState, toParams, fromState, fromParams) {
    if (toState.access === undefined) { return; }
    if (toState.access.requireLogin) {
      if (response && response.session) {
        // авторизация требуется и пользователь авторизован
        // возможная проверка прав
      } else {
        // авторизация требуется и пользователь не авторизован
        if(fromState.name != toState.name && fromState.name){
          $state.go(fromState.name, fromParams, {reload: true});
        } else {
          $state.go('index', {}, {reload: true})
        }
        event.preventDefault();
      }
    }
  }

}]);


App.service('ToastService', ['$mdToast', function($mdToast) {
  this.show = function(status, message, timeout=1000) {
    $mdToast.show({
      hideDelay   : timeout,
      locals      : {status: status, message: message},
      controller  : 'ToastController',
      templateUrl : '/static/templates/dialogs/toast.html',
      position    : 'top right',
    });
  }
}]).controller("ToastController", ['$scope', '$mdToast', '$state', 'status', 'message',
function($scope, $mdToast, $state, status, message){
  if(status == 401) { $state.reload() }
  $scope.status = status.toString()[0];
  $scope.message = message;
  $scope.close = function() { $mdToast.hide() };
}]);


App.service('FlashiesService', ['$rootScope', '$state', 'ToastService',
function($rootScope, $state, ToastService) {
  this.flash = function() {
    $rootScope.$flashies.foreach(function(item){
      ToastService.show(item.status, item.message, 0)
      if (item.target && item.target.post_id){
        $state.go("index.posts.post", {id: item.target.post_id}, {reload: true})
      }
    });
  }
}]);
