App.directive('title', ['$rootScope', '$timeout',
  function($rootScope, $timeout) {
    return {
      link: function() {
        var listener = function(event, toState) {
          $timeout(function() {
            $rootScope.$title = (toState.ncyBreadcrumb.label && toState.ncyBreadcrumb.label)
            ? toState.ncyBreadcrumb.label + ' / Gavrilov Artem Personal Blog! / malltshik'
            : 'Gavrilov Artem Personal Blog! / malltshik';
          });
        };
        $rootScope.$on('$stateChangeSuccess', listener);
      }
    };
  }
]);

App.directive('nanoContainer', function($window, $timeout) {
  return { link: link };
  function link ($scope, element, attrs) {
    var nanoContainer = $(element)[0]
    $(document).ready(function () {
      $(nanoContainer).nanoScroller({
        preventPageScrolling: true, tabIndex: -1, iOSNativeScrolling: true
      });
    });
    var updateScroller = function () {
      $(document).ready(function () {
        $(nanoContainer).nanoScroller();
      });
    }
    function updateSizes () {
      $(element).css('height', $($window).height() - $(element).offset().top);
      updateScroller();
    }

    $($window).on('resize', updateSizes);
    updateSizes();
    setTimeout(updateSizes, 1000);
  };
});

App.directive('autofocus', ['$timeout', function($timeout) {
  return {
    restrict: 'A',
    link : function($scope, $element) {
      $timeout(function() {
        $element[0].focus();
      });
    }
  }
}]);

App.directive('focus', ['$timeout', '$parse', function ($timeout, $parse) {
    return {
        link: function (scope, element, attrs) {
            var expresion = $parse(attrs.focus);
            scope.$watch(expresion, function (value) {
                if (value === true) {
                    $timeout(function () {
                        element[0].focus();
                    });
                }
            });

            element.bind('keypress', function (e) {
              if(e.which === 13) {
                scope.$apply(function() {
                  scope.$eval(attrs.unfocus, {'e': e});
                });
              }
            });
            element.bind('blur', function (e) {
              if(scope.editable != 0) {
                scope.$apply(function() {
                  scope.$eval(attrs.unfocus, {'e': e});
                });
              }
            });
        }
    };
}]);

App.directive('ngCtrlEnter', function() {
  return function(scope, element, attrs) {
    element.bind("keydown keypress", function(e) {
      if((e.which === 13) && e.ctrlKey) {
        scope.$apply(function(){
          scope.$eval(attrs.ngCtrlEnter, {'e': e});
        });
        e.preventDefault();
      }
    });
  };
});

App.directive('ngEsc', function() {
  return function(scope, element, attrs) {
    element.bind("keydown keypress", function(e) {
      if(e.which === 27) {
        scope.$apply(function(){
          scope.$eval(attrs.ngEsc, {'e': e});
        });
        e.preventDefault();
      }
    });
  };
});

App.directive('chipParentBg', ['$timeout', '$parse', function ($timeout, $parse) {
    return {
        link: function (scope, element, attrs) {
            var expresion = $parse(attrs.chipParentBg);
            scope.$watch(expresion, function (value) {
                if (value[2] === true) {
                  $timeout(function () {
                      $(element[0]).parents('md-chip').css('background', value[0] );
                  });
                } else {
                  $timeout(function () {
                      $(element[0]).parents('md-chip').css('background', value[1]);
                  });
                }
            });

        }
    };
}]);

App.directive("animateSkill", [function(){
  return {
    link: function(scope, element, attrs) {
      jQuery(document).ready(function(){
        jQuery(element).find('.skillbar-bar').animate({
          width:jQuery(element).attr('data-percent')
        }, 2000);
      });
    }
  }
}]);


App.directive('scrollOnClick', function() {
  return { restrict: 'A',
    link: function(scope, element, attrs) {
      element.on('click', function(e) {
        $(".nano").nanoScroller({ scrollTo: $(attrs.scrollOnClick) });
      });
    }
  }
});
