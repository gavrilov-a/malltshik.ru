#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Base test case with create new app instance and db."""
from unittest import TestCase
from application import build, db


class BaseTestCase(TestCase):
    """A base test case class."""

    def setUp(self):
        """Create database tables and commit db session."""
        self.client = build('testing').test_client()
        db.create_all()

    def tearDown(self):
        """Teardown database after each test."""
        db.session.remove()
        db.drop_all()
