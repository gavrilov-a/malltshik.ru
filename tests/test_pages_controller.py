from tests import BaseTestCase


class PagesControllerTestCase(BaseTestCase):
    """TestCase class for testing static routers."""

    def test_root_route(self):
        """Test root route."""
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_rest_route(self):
        """Test rest route."""
        response = self.client.get('/rest/')
        self.assertEqual(response.status_code, 200)
