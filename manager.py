import os
from flask_script import Shell, Manager
from flask_migrate import Migrate, MigrateCommand
from application import build, mail
from application.models import db

app = build(os.environ.get("APPLICATION_ENVIRONMENT") or 'default')
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)
manager.add_command('shell', Shell(make_context=lambda: {
        'app': app,
        'db': db,
        'mail': mail
    }))


@manager.command
def clean():
    """Clean all *.pyc files from the project folder."""
    import subprocess
    import os
    clear_pyc = "find . -name *.pyc -delete".split()
    clear_pycache = str(
        "for dir in $(find . -name __pycache__); do rm -r $dir; done")
    print('Find and remove all *.pyc files and __pycache__ dirs..')
    subprocess.call(clear_pyc)
    os.system(clear_pycache)
    return('Done')


@manager.command
def routes():
    """Show all routes ENDPOINT, URL, METHODS."""
    import urllib
    with app.app_context():
        output = []
        for rule in app.url_map.iter_rules():
            methods = ','.join(rule.methods)
            line = urllib.parse.unquote(
                "{:20s} {:20s} {}".format(rule.endpoint, methods, rule))
            output.append(line)
        for line in sorted(output):
            print(line)


@manager.command
def coverage():
    """Make coverage with unit tests."""
    import os
    import unittest
    import coverage
    from coverage.misc import CoverageException
    cov = coverage.coverage(branch=True, include='application/*')
    cov.start()
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)
    cov.stop()
    cov.save()
    print('Coverage Summary:')
    try:
        cov.report()
        basedir = os.path.abspath(os.path.dirname(__file__))
        covdir = os.path.join(basedir, 'tests/coverage')
        cov.html_report(directory=covdir)
        cov.erase()
    except CoverageException as e:
        print(e)
        return exit('No data to report.\n')


@manager.command
def test():
    """Make the unit tests without coverage."""
    import unittest
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)


@manager.command
def deploy():
    """"""
    from application.models.user import User
    from flask import current_app

    username = current_app.config.get('ROOT_USERNAME')
    password = current_app.config.get('ROOT_PASSWORD')
    email = current_app.config.get("ROOT_EMAIL")
    if not username or not password or not email:
        return exit("""Root username or password or email is empty!
Set ROOT_USERNAME and ROOT_PASSWORD variables at config application.""")
    print("Create superuser..")
    user = User()
    user.username = username
    user.password = password
    user.email = email
    user.is_author = True
    db.session.add(user)
    db.session.commit()
    return "Deploy complited!"


if __name__ == '__main__':
    manager.run()
